'use strict'

let state = "Login";

function changeForm () {
    if (state === "Login") {
        $("#form_login").attr('hidden', true);
        $("#form_register").attr('hidden', false);
        state = "Register";
    } else {
        $("#form_login").attr('hidden', false);
        $("#form_register").attr('hidden', true);
        state = "Login";
    }
}

function validateLogin () {
    if ($("#email").val() != "" && $("#password").val() != "") {
        $("#loginBtn").attr('disabled', false);
    }
}

function validateRegister () {
    if ($("#register_email").val() != "" && $("#register_password").val() != "" && 
    $("#confirm_password").val() != "") {
        $("#registerBtn").attr('disabled', false);
    }
}

function login () {
    var settings = {
		"url": baseURL + "api/auth/login",
		"method": "POST",
		"timeout": 0,
		"headers": {
		  "Content-Type": "application/json"
		},
		"data": JSON.stringify({
		  email: $("#email").val(),
          password: $("#password").val()
		}),
	  };
	  
	  $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.access_token) {
            localStorage.setItem("token", response.access_token);
            $("#successAlert").attr('hidden', false).text("Welcome Back!");
            setTimeout(function () {
				window.location.href = 'index.html' }, 3000);
        }
	  }).fail( function (response) {
        console.log('response error', response);
        if (response.responseJSON.error) {
            console.log('Error auth')
            $("#errorAlert").attr('hidden', false).text("Unauthorized user.\nPlease check your email and password.");
            setTimeout(function () {
				$("#errorAlert").attr('hidden', true) }, 3000);
        } else {
            console.log('error else')
            $("#errorAlert").attr('hidden', false).text("Pleaste try again later.");
            setTimeout(function () {
				$("#errorAlert").attr('hidden', true) }, 3000);
        }
      });
}

function register () {
    if ($("#register_password").val() === $("#confirm_password").val() ) {
        var settings = {
            "url": baseURL + "api/user-store",
            "method": "POST",
            "timeout": 0,
            "headers": {
              "Content-Type": "application/json"
            },
            "data": JSON.stringify({
              email: $("#register_email").val(),
              password: $("#register_password").val(),
              user_type: $("#isAdmin").is(':checked')
            }),
          };
          
          $.ajax(settings).done(function (response) {
            console.log(response);
            if (response.status === 201) {
                localStorage.setItem("token", response.access_token);
                $("#successRegisterAlert").attr('hidden', false).text("Welcome!");
                setTimeout(function () {
                    window.location.href = 'index.html' }, 3000);
            } else if (response.error) {
                console.log('Error auth')
                $("#errorRegisterAlert").attr('hidden', false).text("User already exists");
                setTimeout(function () {
                    $("#errorRegisterAlert").attr('hidden', true) }, 3000);
            } else {
                console.log('error else')
                $("#errorRegisterAlert").attr('hidden', false).text("Pleaste try again later.");
                setTimeout(function () {
                    $("#errorRegisterAlert").attr('hidden', true) }, 3000);
            }
          });
    } else {
        $("#errorRegisterAlert").attr('hidden', false).text("Passwords don't match.");
        setTimeout(function () {
            $("#errorRegisterAlert").attr('hidden', true) }, 3000);
    }
}

function navcheck () {
  var settings = {
      "url": baseURL + "api/auth/me",
      "method": "POST",
      "timeout": 0,
      "headers": {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      },
    };
    
    $.ajax(settings).done(function (response) {
      if (response.user_id) {
        window.location.href="index.html";
      }
    });
}
