'use strict'

let bString = '';
const baseImageURL = baseURL + 'assets/images/';

jQuery(document).ready( function () {
    getPosts();
    navcheck();
});

function getPosts () {

    console.log('getPosts');

	var settings = {
        "url": baseURL + "api/favorite-get",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json"
        },
        "data": JSON.stringify({
          user_id: localStorage.getItem('u_id') 
        }),
      };

	$.ajax(settings).done( function (data) {
		console.log(data);
		if (data.status === 200) {
			if (data.data.length > 0) {
                console.log('Exito');
                data.data.forEach(element => {
                    if (element.posts.user) {
                        bString += '<div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">\
                            <img src="'+baseImageURL + element.posts.post_image+'" class="post-image" alt="Image" onclick="makeModal(\''+element.post_image+'\', \''+element.post_title+'\')" >\
                            <h5>'+ element.posts.post_title +'</h5>\
                            <span class="text-muted">BY: '+ element.posts.user.email +'</span>\
                            <p>'+ element.posts.post_description +'</p>\
                        </div>';
                    } else {
                        bString += '<div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">\
                            <img src="'+baseImageURL + element.posts.post_image+'" class="post-image" alt="Image" onclick="makeModal(\''+element.post_image+'\', \''+element.post_title+'\')" >\
                            <h5>'+ element.posts.post_title +'</h5>\
                            <p>'+ element.posts.post_description +'</p>\
                        </div>';
                    }
                });
                document.getElementById("mainContainer").innerHTML+=bString;
                var $grid = $(".grid").masonry({
                    percentPosition: true
                });
                
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry();
                });
            } else {
                document.getElementById("mainContainer").innerHTML+=bString;
                var $grid = $(".grid").masonry({
                    percentPosition: true
                });
                
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry();
                });
            }
		} else {
			alert('Ocurrio un error')
            document.getElementById("mainContainer").innerHTML+=bString;
            var $grid = $(".grid").masonry({
                percentPosition: true
            });
            
            $grid.imagesLoaded().progress( function() {
                $grid.masonry();
            });
		}
	});
}

function makeModal (image, title) {
    $("#modalTitle").text(title);
    $("#modalImage").attr('src', baseImageURL + image);
    $("#postModal").modal('show');
}

function navcheck () {
    var settings = {
        "url": baseURL + "api/auth/me",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.user_id) {
            localStorage.setItem("u_id", response.user_id);
            $("#navMail").text(response.email);
            $("#navLogin").attr('hidden', true);
            $("#navLogout").attr('hidden', false);
            if (response.user_type === 1) {
                $("#navModerate").attr('hidden', false);
            }
        }
      }).fail(function (response) {
		localStorage.removeItem('token');
    	localStorage.removeItem('u_id');
        window.location.href = "index.html";
	  });
}

function logout () {
    localStorage.removeItem('token');
    localStorage.removeItem('u_id');
    location.reload();
}
