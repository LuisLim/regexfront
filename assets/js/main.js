'use strict'

let bString = '';
const baseImageURL = baseURL+'assets/images/';

jQuery(document).ready( function () {
    populateContainer();
    navcheck();
});

function populateContainer () {
    let iString = '';
    for (let index = 0; index < 20; index++) {
        let img = Math.floor(Math.random() * 3);
        switch (img) {
            case 0:
                iString = '1.jpg';
                break;
            case 1:
                iString = '2.png';
                break;
            case 2:
                iString = '3.png';
                break;        
        }
    bString += '<div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">\
                    <img src="'+baseImageURL + iString+'" class="post-image" alt="Image" onclick="makeModal(\''+iString+'\', \'Lorem Ipsum\')" />\
                    <h5>Lorem ipsum</h5>\
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent facilisis dictum metus in pellentesque.</p>\
                    <i class="bi bi-heart-fill" style="margin-right: 3px;"></i><label> 30</label>\
                </div>';
    }
    document.getElementById("mainContainer").innerHTML+=bString;
    var $grid = $(".grid").masonry({
        percentPosition: true
    });
    
    $grid.imagesLoaded().progress( function() {
        $grid.masonry();
    });
};

var pageCounter = 1;

function getPosts () {
    bString = '';

    console.log('getPosts');

    var settings;
    if (pageCounter === 1) {
        settings = {
            "url": baseURL + "api/all-posts",
            "method": "GET",
          };
    } else {
        settings = {
            "url": baseURL + "api/all-posts?page=" + page,
            "method": "GET",
          };
    }

	

	$.ajax(settings).done( function (data) {
		console.log(data);
		if (data.status === 200) {
			if (data.data.data.length > 0) {
                console.log('Exito');
                data.data.data.forEach(element => {
                    if (element.user_id) {
                        bString += '<div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">\
                            <img src="'+baseImageURL + element.post_image+'" class="post-image" alt="Image" onclick="makeModal(\''+element.post_image+'\', \''+element.post_title+'\')" >\
                            <h5>'+ element.post_title +'</h5>\
                            <span class="text-muted">BY: '+ element.user.email +'</span>\
                            <p>'+ element.post_description +'</p>\
                            <div id="fav'+element.post_id+'"><button class="btn btn-outline-dark mb-2" onclick="favorite('+element.post_id+', '+element.fav+')"><i class="bi bi-heart-fill" style="margin-right: 3px;"></i> '+element.fav+' </button></div>\
                            <div hidden id="errorAlert'+element.post_id+'" class="alert alert-danger" role="alert">\
                                Please Access To Favorite It.\
                            </div>\
                        </div>';
                    } else {
                        bString += '<div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">\
                            <img src="'+baseImageURL + element.post_image+'" class="post-image" alt="Image" onclick="makeModal(\''+element.post_image+'\', \''+element.post_title+'\')" >\
                            <h5>'+ element.post_title +'</h5>\
                            <p>'+ element.post_description +'</p>\
                            <div id="fav'+element.post_id+'"><button class="btn btn-outline-dark mb-2" onclick="favorite('+element.post_id+', '+element.fav+')"><i class="bi bi-heart-fill" style="margin-right: 3px;"></i> '+element.fav+' </button></div>\
                            <div hidden id="errorAlert'+element.post_id+'" class="alert alert-danger" role="alert">\
                                Please Access To Favorite It.\
                            </div>\
                        </div>';
                    }
                });
                // $("#mainContainer").append(bString);
                var $bString = $(bString);
                var $grid = $(".grid").masonry({
                    percentPosition: true
                });
                $grid.append($bString).masonry('appended', $bString);
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry();
                });

            }
		} else {
			alert('Ocurrio un error');
		}
	});
    pageCounter++;
}

window.addEventListener('scroll',()=>{
    console.log(window.scrollY) //scrolled from top
    console.log(window.innerHeight) //visible part of screen
    if(window.scrollY + window.innerHeight >= 
    document.documentElement.scrollHeight){
    getPosts();
    }
})

function makeModal (image, title) {
    $("#modalTitle").text(title);
    $("#modalImage").attr('src', baseImageURL + image);
    $("#postModal").modal('show');
}

function navcheck () {
    var settings = {
        "url": baseURL + "api/auth/me",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.user_id) {
            localStorage.setItem("u_id", response.user_id);
            $("#navMail").text(response.email);
            $("#navLogin").attr('hidden', true);
            $("#navLogout").attr('hidden', false);
            $("#navFav").attr('hidden', false);
            if (response.user_type === 1) {
                $("#navModerate").attr('hidden', false);
            }
        }
      }).fail(function (response) {
		localStorage.removeItem('token');
    	localStorage.removeItem('u_id');
	  });
}

function logout () {
    localStorage.removeItem('token');
    localStorage.removeItem('u_id');
    location.reload();
}

function favorite (id, count) {
    if (localStorage.getItem('u_id') != null) {
        var settings = {
            "url": baseURL + "api/favorite-post",
            "method": "POST",
            "timeout": 0,
            "headers": {
              "Content-Type": "application/json"
            },
            "data": JSON.stringify({
              user_id: localStorage.getItem('u_id'),
              post_id: id
            }),
          };
          
          $.ajax(settings).done(function (response) {
            console.log(response);
            if (response.status === 201) {
                console.log('if 201');
                if (response.type === 1 ) {
                    console.log('If 1');
                    count++;
                } else {
                    console.log('if 0');
                    count--;
                }
                console.log('Coount', count);
                document.getElementById("fav"+id).innerHTML = '<button class="btn btn-outline-dark" onclick="favorite('+id+', '+count+')">\
                    <i class="bi bi-heart-fill" style="margin-right: 3px;"></i> '+count+' </button>';
            } else if (response.error) {
                console.log('Error auth')
                $("#errorRegisterAlert").attr('hidden', false).text("User already exists");
                setTimeout(function () {
                    $("#errorRegisterAlert").attr('hidden', true) }, 3000);
            }
          });
    } else {
        $("#errorAlert" + id).attr('hidden', false);
        setTimeout(function () {
            $("#errorAlert" + id).attr('hidden', true) }, 3000);
    }
}