'use strict'

let bString = '';
const baseImageURL = baseURL + 'assets/images/';

jQuery(document).ready( function () {
    getPosts();
    navcheck();
});

function getPosts () {

	var settings = {
		"url": baseURL + "api/moderate-posts",
		"method": "GET",
	  };

	$.ajax(settings).done( function (data) {
		console.log(data);
		if (data.status === 200) {
			if (data.data.length > 0) {
                console.log('Exito');
                data.data.forEach(element => {
                    bString += '<div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">\
                            <img src="'+baseImageURL + element.post_image+'" class="post-image" alt="Image" onclick="makeModal(\''+element.post_image+'\', \''+element.post_title+'\')" >\
                            <h5>'+ element.post_title +'</h5>\
                            <p>'+ element.post_description +'</p>\
                            <button class="btn btn-outline-dark" onclick="allow('+element.post_id+')" style="width: 100% !important;">ALLOW</button>\
                        </div>';
                });
                document.getElementById("mainContainer").innerHTML+=bString;
                var $grid = $(".grid").masonry({
                    percentPosition: true
                });
                
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry();
                });
            } else {
                document.getElementById("mainContainer").innerHTML+=bString;
                var $grid = $(".grid").masonry({
                    percentPosition: true
                });
                
                $grid.imagesLoaded().progress( function() {
                    $grid.masonry();
                });
            }
		} else {
			alert('Ocurrio un error')
            document.getElementById("mainContainer").innerHTML+=bString;
            var $grid = $(".grid").masonry({
                percentPosition: true
            });
            
            $grid.imagesLoaded().progress( function() {
                $grid.masonry();
            });
		}
	});
}

function makeModal (image, title) {
    $("#modalTitle").text(title);
    $("#modalImage").attr('src', baseImageURL + image);
    $("#postModal").modal('show');
}

function navcheck () {
    var settings = {
        "url": baseURL + "api/auth/me",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.user_type === 1) {
            localStorage.setItem("u_id", response.user_id);
            $("#navMail").text(response.email);
            $("#navLogin").attr('hidden', true);
            $("#navLogout").attr('hidden', false);
            $("#navModerate").attr('hidden', false);
            $("#navFav").attr('hidden', false);
        } else {
            window.location.href = 'index.html';
        }
      }).fail(function (response) {
        localStorage.removeItem('token');
        localStorage.removeItem('u_id');
        window.location.href = 'index.html';
      });
}

function logout () {
    localStorage.removeItem('token');
    localStorage.removeItem('u_id');
    location.reload();
}

function allow (id) {
    var settings = {
        "url": baseURL + "api/allow-post",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json"
        },
        "data": JSON.stringify({
          post_id: id 
        }),
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.status === 201) {
            $("#successRegisterAlert").attr('hidden', false).text("Post Allowed!");
            location.reload();
        } else {
            $("#errorRegisterAlert").attr('hidden', false).text("Pleaste try again later.");
            setTimeout(function () {
                $("#errorRegisterAlert").attr('hidden', true) }, 3000);
        }
      });
}