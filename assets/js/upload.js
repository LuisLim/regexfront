jQuery(document).ready( function () {
    navcheck();
});

function previewFile(input){
	var file = $("input[type=file]").get(0).files[0];
    console.log(file.name);
    $("#file_text").text(file.name);
	if(file){
	  var reader = new FileReader();

	  reader.onload = function(){
		  $("#previewImg").attr("src", reader.result);
		  $("#avatarContainer").attr("hidden", false);
	  }

	  reader.readAsDataURL(file);
	}
}

function validate () {
	if ($("#post_title").val() != "" && $("#post_description").val() != ""
	&& $("#post_image").get(0).files.length > 0) {
		$("#uploadBtn").attr('disabled', false);
	} else {
		$("#uploadBtn").attr('disabled', true);
	}
}

function storePost () {
	let postForm = new FormData();
	postForm.append('post_title', $("#post_title").val());
	postForm.append('post_description', $("#post_description").val());
	postForm.append('post_image', $('#post_image').prop("files")[0]);
	postForm.append('user_id', localStorage.getItem('u_id'));

	if (localStorage.getItem('u_id')) {
		postForm.append('post_status', 1);
	} else {
		postForm.append('post_status', 0);
	}

	var settings = {
		"url": baseURL + "api/post-store",
		"method": "POST",
		"timeout": 0,
		"processData": false,
		"mimeType": "multipart/form-data",
		"contentType": false,
		"data": postForm
	  };

	$.ajax(settings).done( function (data) {
		console.log(data);
		let parseData = JSON.parse(data)
		console.log('Parse Data Step 1', parseData);
		if (parseData.status === 201) {
			$("#successAlert").attr('hidden', false);
			setTimeout(function () {
				window.location.href = 'index.html' }, 3000);
		} else {
			$("#errorAlert").attr('hidden', false);
			setTimeout(function () {
				window.location.href = 'index.html' }, 3000);
		}
	});
}

function navcheck () {
    var settings = {
        "url": baseURL + "api/auth/me",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.user_id) {
            localStorage.setItem("u_id", response.user_id);
            $("#navMail").text(response.email);
            $("#navLogin").attr('hidden', true);
            $("#navLogout").attr('hidden', false);
			$("#navFav").attr('hidden', false);
			if (response.user_type === 1) {
                $("#navModerate").attr('hidden', false);
            }
        }
      }).fail(function (response) {
		localStorage.removeItem('token');
    	localStorage.removeItem('u_id');
	  });
}

function logout () {
    localStorage.removeItem('token');
    localStorage.removeItem('u_id');
    location.reload();
}
